class Book < ApplicationRecord
  belongs_to :author
  belongs_to :categorie
  has_many :reservations
end
