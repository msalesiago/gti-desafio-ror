class Reservation < ApplicationRecord
  belongs_to :client
  belongs_to :book
  belongs_to :librarian

  before_create :decrease_estoque
  after_destroy :increase_estoque

  private

  def decrease_estoque
    if self.book.stock < 1
      throw(:abort)
    end
    self.book.update(stock: book.stock-1)
    end
  end

  def increase_estoque
    self.book.update(stock: book.stock+1)
end

