# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



10.times do 
    Categorie.create!(name: Faker::Book.unique.genre)
    Author.create!(name: Faker::Book.unique.author)
    Client.create!(name: Faker::Name.unique.name)
end
puts "Gerado Catergorias, autores e clientes"

10.times do 
    Book.create!(name: Faker::Book.unique.title,
                 stock: Faker::Number.unique.between(from: 0, to: 15),
                 author: Author.all.sample,
                 categorie: Categorie.all.sample)
end
puts "Gerado Livros"

Librarian.create!(email:"admin@admin.com")

4.times do 
    Reservation.create!(book: Book.all.sample,
                        client: Client.all.sample,
                        librarian: Librarian.all.sample)
end
puts "Gerado Reservas"


User.create!(:email => 'admin@admin.com',:password => '123456',:password_confirmation => '123456')
